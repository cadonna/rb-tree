/*
 * rb-tree-tmpl.hpp
 *
 *  Created on: May 20, 2015
 *      Author: bruno
 */

#ifndef RB_TREE_TMPL_HPP_
#define RB_TREE_TMPL_HPP_

#include <memory>

/**
 * Type for the color of the nodes in a red-black tree
 */
enum rb_color
	: bool
	{
		red = true, black = false
};

/**
 * Type for the node of a red-black tree
 */
template <typename K>
struct rb_node
{
	K key;
	rb_color color;
	std::shared_ptr<rb_node<K> > parent;
	std::shared_ptr<rb_node<K> > left;
	std::shared_ptr<rb_node<K> > right;
	rb_node(void) :
			key(), color(black), parent(std::shared_ptr<rb_node<K> >(nullptr)), left(
					std::shared_ptr<rb_node<K> >(nullptr)), right(
					std::shared_ptr<rb_node<K> >(nullptr))
	{
	}
	;
	rb_node(const rb_node<K>& node) :
			key(node.key), color(node.color), parent(node.parent), left(
					node.left), right(node.right)
	{
	}
	;
};

/**
 * Compares two nodes of a red-black tree.
 *
 * @param node on the left hand side
 * @param node on the right hand side
 * @return true if the operator== returns true for all members of the nodes,
 * 		   false otherwise
 */
template <typename K>
bool operator ==(const rb_node<K>& lhs, const rb_node<K>& rhs)
{
	if (&lhs == &rhs) {
		return true;
	}
	else {
		return (lhs.key == rhs.key
				 &&
				 lhs.color == rhs.color
				 &&
				 lhs.parent == rhs.parent
				 &&
				 lhs.left == rhs.left
				 &&
				 lhs.right == rhs.right);
	}
}


template <typename K>
class rb_tree
{

private:

	/**
	 * Sentinel of the red-black tree.
	 *
	 * The sentinel represents all NILs in the red-black tree
	 * -- all leaves and the root's parent.
	 */
	std::shared_ptr<rb_node<K> > nil;

	/**
	 * Root of the red-black tree.
	 */
	std::shared_ptr<rb_node<K> > root;

	/**
	 * Counts the nodes in the red-black tree.
	 */
	std::size_t node_cnt;

	/**
	 * Computes recursively the height of the tree rooted at a node.
	 *
	 * The height of the red-black tree is the number of nodes on
	 * the longest path from the root to a leave minus one (i.e. not including the root).
	 *
	 * @param root of the tree
	 * @return height of tree rooted at the given node
	 */
	std::size_t compute_height(const std::shared_ptr<rb_node<K> > node) const;

	/**
	 * Restores the red-black properties of the red-black tree after
	 * insertion of a node.
	 *
	 * @param node to insert
	 */
	void fix_up(std::shared_ptr<rb_node<K> > node);

	/**
	 * Rotates a node to the left.
	 *
	 * @param node to rotate
	 */
	void left_rotate(std::shared_ptr<rb_node<K> > node);

	/**
	 * Rotates a node to the right.
	 *
	 * @param node to rotate
	 */
	void right_rotate(std::shared_ptr<rb_node<K> > node);

	/**
	 * Fetches recursively nodes with a given key from the red-black tree.
	 *
	 * @param node from which to start
	 * @param key
	 * @return found node
	 */
	const std::shared_ptr<const rb_node<K> > do_fetch(const std::shared_ptr<rb_node<K> > node, const K key) const;

public:

	/**
	 * Constructor
	 */
	rb_tree(void) :
			nil(std::make_shared<rb_node<K> >()), root(
					std::shared_ptr<rb_node<K> >(nil)), node_cnt(0)
	{
	}

	/**
	 * Gets the number of nodes in the red-black tree.
	 */
	std::size_t size(void) const
	{
		return node_cnt;
	}

	/**
	 * @brief Gets the height of the red-black tree.
	 *
	 * The height of the red-black tree is the number of nodes on
	 * the longest path from the root to a leave minus one (i.e. not including the root).
	 *
	 * @return height of the red-black tree
	 */
	std::size_t height(void) const;

	/**
	 * Gets the root of the red-black tree.
	 *
	 * @return root of the red-black tree
	 */
	const std::shared_ptr<rb_node<K> >& get_root() const
	{
		return this->root;
	}

	/**
	 * Gets the NIL pointer of the red-black tree.
	 *
	 * @return NIL pointer of the red-black tree
	 */
	const std::shared_ptr<rb_node<K> >& get_nil() const
	{
		return this->nil;
	}

	/**
	 * @brief Gets the black-height of a node.
	 *
	 * The black-height of a node is the number of black nodes on
	 * any simple path from, but not including, the node.
	 *
	 * @param node whose black-height should be computed
	 * @return black-height of the given node
	 */
	std::size_t black_height(const std::shared_ptr<rb_node<K> > node) const;

	/**
	 * Inserts a node into the red-black tree.
	 *
	 * @param node to insert
	 * @return true if node could be inserted because key is not already in tree,
	 *         false otherwise
	 */
	bool insert(const rb_node<K>& node);

	/**
	 * Fetches nodes with a given key from the red-black tree.
	 *
	 * @param key to look for
	 * @return found node
	 */
	const std::shared_ptr<const rb_node<K> > fetch(const K key) const;

};

/**
 * see rb_tree
 */
template <typename K>
std::size_t rb_tree<K>::height(void) const {
	return this->node_cnt == 0 ? 0 : compute_height(this->root) - 1;
}

/**
 * see rb_tree
 */
template <typename K>
std::size_t rb_tree<K>::compute_height(const std::shared_ptr<rb_node<K> > node) const {

	if (node == this->nil) {

		return 0;
	}
	else {

		return std::max<std::size_t>(compute_height(node->left), compute_height(node->right)) + 1;
	}
}

/**
 * see rb_tree
 */
template <typename K>
std::size_t rb_tree<K>::black_height(const std::shared_ptr<rb_node<K> > node) const {

	if (node == this->nil || node == nullptr) {

		return 0;
	}
	else {

		std::size_t lcnt = black_height(node->left);
		std::size_t rcnt = black_height(node->right);

		if (node->color == red) {
			return ++lcnt;
		}
		else if (node->left->color == red) {
			return lcnt;
		}
		else if (node->right->color == red) {
			return rcnt;
		}
		else {
			return ++lcnt;
		}
	}
}

/**
 * see rb_tree
 */
template <typename K>
bool rb_tree<K>::insert(const rb_node<K>& node) {

	std::shared_ptr<rb_node<K> > _node = std::make_shared<rb_node<K> >(node);
	std::shared_ptr<rb_node<K> > parent = this->nil;
	std::shared_ptr<rb_node<K> > i = this->root;

	// find parent to append the node
	while (i != this->nil) {
		parent = i;
		if (_node->key < i->key) {
			i = i->left;
		}
		else {
			i = i->right;
		}
	}
	_node->parent = parent;

	// tree is empty
	if (parent == this->nil) {
		this->root = _node;
	}
	// key of node less than key of parent
	else if (_node->key < parent->key) {
		parent->left = _node;
	}
	else if (_node->key > parent->key) {
		parent->right = _node;
	}
	else {
		return false;
	}
	_node->left = this->nil;
	_node->right = this->nil;
	_node->color = red;

	fix_up(_node);
	++this->node_cnt;

	return true;
}

/**
 * see rb_tree<K>
 */
template <typename K>
void rb_tree<K>::fix_up(std::shared_ptr<rb_node<K> > node)
{
	// while parent is red
	while (node->parent->color == red) {

		// node is left grandchild of grandparent
		if (node->parent == node->parent->parent->left) {

			std::shared_ptr<rb_node<K> > uncle = node->parent->parent->right;

			// uncle is red
			if (uncle->color == red) {

				// set parent black
				node->parent->color = black;
				// set uncle black
				uncle->color = black;
				// set grandparent red
				node->parent->parent->color = red;

				node = node->parent->parent;
			}

			// uncle is black
			else {

				// node is right child
				if (node == node->parent->right) {

					node = node->parent;
					left_rotate(node);
				}

				node->parent->color = black;
				node->parent->parent->color = red;
				right_rotate(node->parent->parent);
			}
		}

		// node is right grandchild of grandparent
		else {

			std::shared_ptr<rb_node<K> > uncle = node->parent->parent->left;

			// uncle is red
			if (uncle->color == red) {

				// set parent black
				node->parent->color = black;
				// set uncle black
				uncle->color = black;
				// set grandparent red
				node->parent->parent->color = red;

				node = node->parent->parent;
			}

			// uncle is black
			else {

				// node is left child
				if (node == node->parent->left) {

					node = node->parent;
					right_rotate(node);
				}

				node->parent->color = black;
				node->parent->parent->color = red;
				left_rotate(node->parent->parent);
			}
		}
	}
	this->root->color = black;
}


/**
 * see rb_tree<K>
 */
template <typename K>
void rb_tree<K>::left_rotate(std::shared_ptr<rb_node<K> > node) {

	// get right child
	std::shared_ptr<rb_node<K> > right_child = node->right;

	// turn left subtree of right child into right subtree of the node
	node->right = right_child->left;
	if (right_child->left != this->nil) {
		right_child->left->parent = node;
	}

	// parent of the node becomes parent of its right child
	right_child->parent = node->parent;
	if (node->parent == this->nil) {
		this->root = right_child;
	}
	else if (node == node->parent->left) {
		node->parent->left = right_child;
	}
	else {
		node->parent->right = right_child;
	}

	// put node as left child of its right child
	right_child->left = node;
	node->parent = right_child;
}


/**
 * see rb_tree<K>
 */
template <typename K>
void rb_tree<K>::right_rotate(std::shared_ptr<rb_node<K> > node) {

	// get left child of the node
	std::shared_ptr<rb_node<K> > left_child = node->left;

	// turn right subtree of left child into left subtree of the node
	node->left = left_child->right;
	if (left_child->right != this->nil) {
		left_child->right->parent = node;
	}

	// parent of the node becomes parent of its left child
	left_child->parent = node->parent;
	if (node->parent == this->nil) {
		this->root = left_child;
	}
	else if (node == node->parent->right) {
		node->parent->right = left_child;
	}
	else {
		node->parent->left = left_child;
	}

	// put node as right child of its left child
	left_child->right = node;
	node->parent = left_child;
}

/**
 * see rb_tree
 */
template <typename K>
const std::shared_ptr<const rb_node<K> > rb_tree<K>::fetch(const K key) const
{
	return do_fetch(this->root, key);
}


/**
 * see rb_tree
 */
template <typename K>
const std::shared_ptr<const rb_node<K> > rb_tree<K>::do_fetch(const std::shared_ptr<rb_node<K> > node, const K key) const
{
	if (node == this->nil) {

		return this->nil;
	}
	else {
		if (key < node->key) {
			return do_fetch(node->left, key);
		}
		else if (key > node->key) {
			return do_fetch(node->right, key);
		}
		else {
			return node;
		}
	}
}

#endif /* RB_TREE_TMPL_HPP_ */
