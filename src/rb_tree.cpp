/*
 * rb_tree.cpp
 *
 *  Created on: May 15, 2015
 *      Author: bruno
 */

#include "rb_tree.hpp"

std::size_t rb_tree::black_height(const std::shared_ptr<rb_node> node) const {

	if (node == this->nil || node == nullptr) {

		return 0;
	}
	else {

		std::size_t lcnt = black_height(node->left);
		std::size_t rcnt = black_height(node->right);

		if (node->color == red) {
			return ++lcnt;
		}
		else if (node->left->color == red) {
			return lcnt;
		}
		else if (node->right->color == red) {
			return rcnt;
		}
		else {
			return ++lcnt;
		}
	}
}

/**
 * see rb_node
 */
bool operator==(const rb_node& lhs, const rb_node& rhs)
{
	if (&lhs == &rhs) {
		return true;
	}
	else {
		return (lhs.key == rhs.key
				&&
				lhs.color == rhs.color
				&&
				lhs.parent == rhs.parent
				&&
				lhs.left == rhs.left
				&&
				lhs.right == rhs.right);
	}
}

/**
 * see rb_tree
 */
bool rb_tree::insert(const rb_node& node) {

	std::shared_ptr<rb_node> _node = std::make_shared<rb_node>(node);
	std::shared_ptr<rb_node> parent = this->nil;
	std::shared_ptr<rb_node> i = this->root;

	// find parent to append the node
	while (i != this->nil) {
		parent = i;
		if (_node->key < i->key) {
			i = i->left;
		}
		else {
			i = i->right;
		}
	}
	_node->parent = parent;

	// tree is empty
	if (parent == this->nil) {
		this->root = _node;
	}
	// key of node less than key of parent
	else if (_node->key < parent->key) {
		parent->left = _node;
	}
	else if (_node->key > parent->key) {
		parent->right = _node;
	}
	else {
		return false;
	}
	_node->left = this->nil;
	_node->right = this->nil;
	_node->color = red;

	fix_up(_node);
	++this->node_cnt;

	return true;
}

/**
 * see rb_tree
 */
void rb_tree::fix_up(std::shared_ptr<rb_node> node)
{
	// while parent is red
	while (node->parent->color == red) {

		// node is left grandchild of grandparent
		if (node->parent == node->parent->parent->left) {

			std::shared_ptr<rb_node> uncle = node->parent->parent->right;

			// uncle is red
			if (uncle->color == red) {

				// set parent black
				node->parent->color = black;
				// set uncle black
				uncle->color = black;
				// set grandparent red
				node->parent->parent->color = red;

				node = node->parent->parent;
			}

			// uncle is black
			else {

				// node is right child
				if (node == node->parent->right) {

					node = node->parent;
					left_rotate(node);
				}

				node->parent->color = black;
				node->parent->parent->color = red;
				right_rotate(node->parent->parent);
			}
		}

		// node is right grandchild of grandparent
		else {

			std::shared_ptr<rb_node> uncle = node->parent->parent->left;

			// uncle is red
			if (uncle->color == red) {

				// set parent black
				node->parent->color = black;
				// set uncle black
				uncle->color = black;
				// set grandparent red
				node->parent->parent->color = red;

				node = node->parent->parent;
			}

			// uncle is black
			else {

				// node is left child
				if (node == node->parent->left) {

					node = node->parent;
					right_rotate(node);
				}

				node->parent->color = black;
				node->parent->parent->color = red;
				left_rotate(node->parent->parent);
			}
		}
	}
	this->root->color = black;
}


/**
 * see rb_tree
 */
void rb_tree::left_rotate(std::shared_ptr<rb_node> node) {

	// get right child
	std::shared_ptr<rb_node> right_child = node->right;

	// turn left subtree of right child into right subtree of the node
	node->right = right_child->left;
	if (right_child->left != this->nil) {
		right_child->left->parent = node;
	}

	// parent of the node becomes parent of its right child
	right_child->parent = node->parent;
	if (node->parent == this->nil) {
		this->root = right_child;
	}
	else if (node == node->parent->left) {
		node->parent->left = right_child;
	}
	else {
		node->parent->right = right_child;
	}

	// put node as left child of its right child
	right_child->left = node;
	node->parent = right_child;
}


/**
 * see rb_tree
 */
void rb_tree::right_rotate(std::shared_ptr<rb_node> node) {

	// get left child of the node
	std::shared_ptr<rb_node> left_child = node->left;

	// turn right subtree of left child into left subtree of the node
	node->left = left_child->right;
	if (left_child->right != this->nil) {
		left_child->right->parent = node;
	}

	// parent of the node becomes parent of its left child
	left_child->parent = node->parent;
	if (node->parent == this->nil) {
		this->root = left_child;
	}
	else if (node == node->parent->right) {
		node->parent->right = left_child;
	}
	else {
		node->parent->left = left_child;
	}

	// put node as right child of its left child
	left_child->right = node;
	node->parent = left_child;
}


/**
 * see rb_tree
 */
std::size_t rb_tree::compute_height(const std::shared_ptr<rb_node> node) const {

	if (node == this->nil) {

		return 0;
	}
	else {

		return std::max<std::size_t>(compute_height(node->left), compute_height(node->right)) + 1;
	}
}

/**
 * see rb_tree
 */
std::size_t rb_tree::height(void) const {
	return this->node_cnt == 0 ? 0 : compute_height(this->root) - 1;
}

/**
 * see rb_tree
 */
const std::shared_ptr<const rb_node> rb_tree::fetch(const int key) const
{
	return do_fetch(this->root, key);
}


/**
 * see rb_tree
 */
const std::shared_ptr<const rb_node> rb_tree::do_fetch(const std::shared_ptr<rb_node> node, const int key) const
{
	if (node == this->nil) {

		return this->nil;
	}
	else {
		if (key < node->key) {
			return do_fetch(node->left, key);
		}
		else if (key > node->key) {
			return do_fetch(node->right, key);
		}
		else {
			return node;
		}
	}
}
