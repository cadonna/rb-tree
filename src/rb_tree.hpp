/*
 * Red Black Tree
 *
 *  Created on: May 14, 2015
 *      Author: bruno
 */

#ifndef RB_TREE_HPP_
#define RB_TREE_HPP_

#include <memory>
#include <list>

/**
 * Type for the color of the nodes in a red-black tree
 */
enum rb_color
	: bool
	{
		red = true, black = false
};

/**
 * Type for the node of a red-black tree
 */
struct rb_node
{
	int key;
	rb_color color;
	std::shared_ptr<rb_node> parent;
	std::shared_ptr<rb_node> left;
	std::shared_ptr<rb_node> right;
	rb_node(void) :
			key(0), color(black), parent(std::shared_ptr<rb_node>(nullptr)), left(
					std::shared_ptr<rb_node>(nullptr)), right(
					std::shared_ptr<rb_node>(nullptr))
	{
	}
	;
	rb_node(const rb_node& node) :
			key(node.key), color(node.color), parent(node.parent), left(
					node.left), right(node.right)
	{
	}
	;
};

bool operator==(const rb_node& lhs, const rb_node& rhs);

/**
 * @brief Red-black tree
 */
class rb_tree
{

private:

	/**
	 * Sentinel of the red-black tree.
	 *
	 * The sentinel represents all NILs in the red-black tree
	 * -- all leaves and the root's parent.
	 */
	std::shared_ptr<rb_node> nil;

	/**
	 * Root of the red-black tree.
	 */
	std::shared_ptr<rb_node> root;

	/**
	 * Counts the nodes in the red-black tree.
	 */
	std::size_t node_cnt;

	/**
	 * Computes recursively the height of the tree rooted at a node.
	 *
	 * The height of the red-black tree is the number of nodes on
	 * the longest path from the root to a leave minus one (i.e. not including the root).
	 *
	 * @param root of the tree
	 * @return height of tree rooted at the given node
	 */
	std::size_t compute_height(const std::shared_ptr<rb_node> node) const;

	/**
	 * Restores the red-black properties of the red-black tree after
	 * insertion of a node.
	 *
	 * @param node to insert
	 */
	void fix_up(std::shared_ptr<rb_node> node);

	/**
	 * Rotates a node to the left.
	 *
	 * @param node to rotate
	 */
	void left_rotate(std::shared_ptr<rb_node> node);

	/**
	 * Rotates a node to the right.
	 *
	 * @param node to rotate
	 */
	void right_rotate(std::shared_ptr<rb_node> node);

	/**
	 * Fetches recursively nodes with a given key from the red-black tree.
	 *
	 * @param node from which to start
	 * @param key
	 * @return found node
	 */
	const std::shared_ptr<const rb_node> do_fetch(const std::shared_ptr<rb_node> node, const int key) const;

public:

	/**
	 * Constructor
	 */
	rb_tree(void) :
			nil(std::make_shared<rb_node>()), root(
					std::shared_ptr<rb_node>(nil)), node_cnt(0)
	{
	}

	/**
	 * Gets the number of nodes in the red-black tree.
	 */
	std::size_t size(void) const
	{
		return node_cnt;
	}

	/**
	 * @brief Gets the height of the red-black tree.
	 *
	 * The height of the red-black tree is the number of nodes on
	 * the longest path from the root to a leave minus one (i.e. not including the root).
	 *
	 * @return height of the red-black tree
	 */
	std::size_t height(void) const;

	/**
	 * Gets the root of the red-black tree.
	 *
	 * @return root of the red-black tree
	 */
	const std::shared_ptr<rb_node>& get_root() const
	{
		return this->root;
	}

	/**
	 * Gets the NIL pointer of the red-black tree.
	 *
	 * @return NIL pointer of the red-black tree
	 */
	const std::shared_ptr<rb_node>& get_nil() const
	{
		return this->nil;
	}

	/**
	 * @brief Gets the black-height of a node.
	 *
	 * The black-height of a node is the number of black nodes on
	 * any simple path from, but not including, the node.
	 *
	 * @param node whose black-height should be computed
	 * @return black-height of the given node
	 */
	std::size_t black_height(const std::shared_ptr<rb_node> node) const;

	/**
	 * Inserts a node into the red-black tree.
	 *
	 * @param node to insert
	 * @return true if node could be inserted because key is not already in tree,
	 *         false otherwise
	 */
	bool insert(const rb_node& node);

	/**
	 * Fetches nodes with a given key from the red-black tree.
	 *
	 * @param key to look for
	 * @return found node
	 */
	const std::shared_ptr<const rb_node> fetch(const int key) const;

};

#endif /* RB_TREE_HPP_ */
