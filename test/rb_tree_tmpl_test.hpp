/*
 * rb_tree_tmpl_test.hpp
 *
 *  Created on: May 20, 2015
 *      Author: bruno
 */

#ifndef RB_TREE_TMPL_TEST_HPP_
#define RB_TREE_TMPL_TEST_HPP_

/**
 * Checks red-black properties of a red-black tree.
 *
 * @param red-black tree to check
 */
template <typename K>
static void check_red_black_props(const rb_tree<K>& tree) {

	// red-black property: the root is black
	BOOST_CHECK( tree.size() == 0 || tree.get_root()->color == black );

	// red-black property: every leaf node is black
	BOOST_CHECK_EQUAL( tree.get_nil()->color, black );

	// red-black property: red nodes must have black children
	BOOST_CHECK( have_red_nodes_black_children(tree) );

	// red-black property: for each node, all simple paths to leaf nodes contain the same number of black nodes
	BOOST_CHECK( have_simple_paths_same_number_of_black_nodes(tree) );

	// consequence of red-black properties:
	// A red-black tree with n internal nodes has height at most 2*log2(n + 1)
	// cf. Cormen et al.: Introduction to Algorithms
	BOOST_CHECK( !(tree.height() > 2*std::log2(tree.size() + 1)) );

}

/**
 * Checks that red nodes have exclusively black children.
 *
 * @param
 * @return true if all red nodes have exclusively black children, false otherwise
 */
template <typename K>
static bool check_red_nodes(const std::shared_ptr<rb_node<K> > node, const std::shared_ptr<rb_node<K> > nil) {

	if (node == nil) {

		return true;
	}
	else {

		// red-black property:
		// red nodes must have black children
		if (node->color == red) {

			if (node->left->color != black
				||
				node->right->color != black) {

				return false;
			}
		}

		return check_red_nodes(node->left, nil) && check_red_nodes(node->right, nil);
	}
}

/**
 * Checks that all red nodes in the red-black tree have exclusively black children.
 *
 * @param red-black tree
 * @return true if all red nodes have exclusively black children, false otherwise
 */
template <typename K>
static bool have_red_nodes_black_children(const rb_tree<K>& tree) {

	return check_red_nodes(tree.get_root(), tree.get_nil());
}

template <typename K>
static bool check_paths(const std::shared_ptr<rb_node<K> > node, int& cnt, const std::shared_ptr<rb_node<K> > nil) {

	if (node == nil) {

		cnt = 0;
		return true;
	}
	else {

		int lcnt = 0;
		int rcnt = 0;
		bool lchk = check_paths(node->left, lcnt, nil);
		bool rchk = check_paths(node->right, rcnt, nil);

		// no problems in left and right subtrees
		if (!lchk || !rchk) {
			return false;
		}

		// red-black property:
		// all simple paths from a node to descendant leaves
		// contain the same number of black nodes
		if (lcnt != rcnt) {
			return false;
		}

		// increase count of black nodes
		cnt = lcnt;
		if (node->color == black) {
			++cnt;
		}

		return true;
	}
}

template <typename K>
static bool have_simple_paths_same_number_of_black_nodes(const rb_tree<K>& tree) {

	int n = 0;
	return check_paths(tree.get_root(), n, tree.get_nil());
}

#endif /* RB_TREE_TMPL_TEST_HPP_ */
