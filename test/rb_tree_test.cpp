/*
 * rb_tree_test.cpp
 *
 *  Created on: May 14, 2015
 *      Author: bruno
 */

#include <iostream>
#include <cmath>
#include <boost/test/unit_test.hpp>
#include "../src/rb_tree.hpp"

static void check_red_black_props(const rb_tree& tree);
static bool have_red_nodes_black_children(const rb_tree& tree);
static bool have_simple_paths_same_number_of_black_nodes(const rb_tree& tree);

BOOST_AUTO_TEST_CASE( node_constructors_tests )
{
	rb_node node;
	BOOST_CHECK_EQUAL(node.key, 0);
	BOOST_CHECK_EQUAL(node.color, black);
	BOOST_CHECK(node.parent == nullptr);
	BOOST_CHECK(node.left == nullptr);
	BOOST_CHECK(node.right == nullptr);
}

BOOST_AUTO_TEST_CASE( node_equality_op_tests )
{
	rb_node node1;
	rb_node node2;
	BOOST_CHECK(node1 == node2);
	BOOST_CHECK(node1 == node1);

	rb_node node3;
	node3.key = 1;
	BOOST_CHECK(!(node3 == node1));

	rb_node node4;
	node4.color = red;
	BOOST_CHECK(!(node4 == node1));

	std::shared_ptr<rb_node> left = std::make_shared<rb_node>();
	rb_node node5;
	node5.left = left;
	BOOST_CHECK(!(node5 == node1));

	std::shared_ptr<rb_node> right = std::make_shared<rb_node>();
	rb_node node6;
	node6.right = right;
	BOOST_CHECK(!(node6 == node1));
}

BOOST_AUTO_TEST_CASE( tree_constructors_tests )
{
	rb_tree tree;
	BOOST_CHECK_EQUAL(tree.size(), 0);
	BOOST_CHECK_EQUAL(tree.height(), 0);
	BOOST_CHECK_EQUAL(tree.black_height(tree.get_root()), 0);
	BOOST_CHECK(tree.get_root() == tree.get_nil());

	// check red-black properties
	check_red_black_props(tree);
}

BOOST_AUTO_TEST_CASE( insert_single_node_test)
{
	// create red-black tree
	rb_tree tree;
	rb_node node;

	// insert data
	int key1 = 2;
	node.key = key1;
	tree.insert(node);

	// check number of nodes in tree
	BOOST_CHECK_EQUAL( tree.size(), 1 );

	// check root
	BOOST_CHECK_EQUAL( tree.get_root()->key, key1 );
	BOOST_CHECK_EQUAL( tree.get_root()->parent, tree.get_nil() );
	BOOST_CHECK_EQUAL( tree.get_root()->left, tree.get_nil() );
	BOOST_CHECK_EQUAL( tree.get_root()->right, tree.get_nil() );

	// check red-black properties
	check_red_black_props(tree);
}

BOOST_AUTO_TEST_CASE( insert_check_return_value)
{
	// create red-black tree
	rb_tree tree;
	rb_node node;

	// insert data
	int key1 = 2;
	node.key = key1;
	bool ok1 = tree.insert(node);
	bool ok2 = tree.insert(node);

	// check
	BOOST_CHECK( ok1 );
	BOOST_CHECK( !ok2 );
}

BOOST_AUTO_TEST_CASE( insert_left_child_right_uncle_red )
{
	// create red-black tree
	rb_tree tree;
	rb_node node;

	// insert data
	int key1 = 5;
	node.key = key1;
	tree.insert(node);

	int key2 = 4;
	node.key = key2;
	tree.insert(node);

	int key3 = 6;
	node.key = key3;
	tree.insert(node);

	int key4 = 3;
	node.key = key4;
	tree.insert(node);

	// check number of nodes in tree
	BOOST_CHECK_EQUAL( tree.size(), 4 );
	BOOST_CHECK_EQUAL( tree.height(), 2 );

	// check red-black properties
	check_red_black_props(tree);
}

BOOST_AUTO_TEST_CASE( insert_right_child_right_uncle_red )
{
	// create red-black tree
	rb_tree tree;
	rb_node node;

	// insert data
	int key1 = 5;
	node.key = key1;
	tree.insert(node);

	int key2 = 3;
	node.key = key2;
	tree.insert(node);

	int key3 = 6;
	node.key = key3;
	tree.insert(node);

	int key4 = 4;
	node.key = key4;
	tree.insert(node);

	// check number of nodes in tree
	BOOST_CHECK_EQUAL( tree.size(), 4 );
	BOOST_CHECK_EQUAL( tree.height(), 2 );

	// check red-black properties
	check_red_black_props(tree);
}

BOOST_AUTO_TEST_CASE( insert_right_child_left_uncle_red )
{
	// create red-black tree
	rb_tree tree;
	rb_node node;

	// insert data
	int key1 = 5;
	node.key = key1;
	tree.insert(node);

	int key2 = 4;
	node.key = key2;
	tree.insert(node);

	int key3 = 6;
	node.key = key3;
	tree.insert(node);

	int key4 = 7;
	node.key = key4;
	tree.insert(node);

	// check number of nodes in tree
	BOOST_CHECK_EQUAL( tree.size(), 4 );
	BOOST_CHECK_EQUAL( tree.height(), 2 );

	// check red-black properties
	check_red_black_props(tree);
}

BOOST_AUTO_TEST_CASE( insert_left_child_left_uncle_red )
{
	// create red-black tree
	rb_tree tree;
	rb_node node;

	// insert data
	int key1 = 5;
	node.key = key1;
	tree.insert(node);

	int key2 = 4;
	node.key = key2;
	tree.insert(node);

	int key3 = 7;
	node.key = key3;
	tree.insert(node);

	int key4 = 6;
	node.key = key4;
	tree.insert(node);

	// check number of nodes in tree
	BOOST_CHECK_EQUAL( tree.size(), 4 );
	BOOST_CHECK_EQUAL( tree.height(), 2 );

	// check red-black properties
	check_red_black_props(tree);
}

BOOST_AUTO_TEST_CASE( insert_left_child_left_uncle_black_grandparent_root )
{
	// create red-black tree
	rb_tree tree;
	rb_node node;

	// insert data
	int key1 = 5;
	node.key = key1;
	tree.insert(node);

	int key2 = 7;
	node.key = key2;
	tree.insert(node);

	int key3 = 6;
	node.key = key3;
	tree.insert(node);

	// check number of nodes in tree
	BOOST_CHECK_EQUAL( tree.size(), 3 );
	BOOST_CHECK_EQUAL( tree.height(), 1 );

	// check red-black properties
	check_red_black_props(tree);
}

BOOST_AUTO_TEST_CASE( insert_right_child_left_uncle_black_grandparent_root )
{
	// create red-black tree
	rb_tree tree;
	rb_node node;

	// insert data
	int key1 = 5;
	node.key = key1;
	tree.insert(node);

	int key2 = 6;
	node.key = key2;
	tree.insert(node);

	int key3 = 7;
	node.key = key3;
	tree.insert(node);

	// check number of nodes in tree
	BOOST_CHECK_EQUAL( tree.size(), 3 );
	BOOST_CHECK_EQUAL( tree.height(), 1 );

	// check red-black properties
	check_red_black_props(tree);
}

BOOST_AUTO_TEST_CASE( insert_left_child_right_uncle_black_grandparent_root )
{
	// create red-black tree
	rb_tree tree;
	rb_node node;

	// insert data
	int key1 = 5;
	node.key = key1;
	tree.insert(node);

	int key2 = 4;
	node.key = key2;
	tree.insert(node);

	int key3 = 3;
	node.key = key3;
	tree.insert(node);

	// check number of nodes in tree
	BOOST_CHECK_EQUAL( tree.size(), 3 );
	BOOST_CHECK_EQUAL( tree.height(), 1 );

	// check red-black properties
	check_red_black_props(tree);
}

BOOST_AUTO_TEST_CASE( insert_right_child_right_uncle_black_grandparent_root )
{
	// create red-black tree
	rb_tree tree;
	rb_node node;

	// insert data
	int key1 = 5;
	node.key = key1;
	tree.insert(node);

	int key2 = 3;
	node.key = key2;
	tree.insert(node);

	int key3 = 4;
	node.key = key3;
	tree.insert(node);

	// check number of nodes in tree
	BOOST_CHECK_EQUAL( tree.size(), 3 );
	BOOST_CHECK_EQUAL( tree.height(), 1 );

	// check red-black properties
	check_red_black_props(tree);
}

BOOST_AUTO_TEST_CASE( insert_left_child_right_uncle_black_grandparent_not_root )
{
	// create red-black tree
	rb_tree tree;
	rb_node node;

	// insert data
	int key1 = 10;
	node.key = key1;
	tree.insert(node);

	int key2 = 8;
	node.key = key2;
	tree.insert(node);

	int key3 = 6;
	node.key = key3;
	tree.insert(node);

	int key4 = 4;
	node.key = key4;
	tree.insert(node);

	int key5 = 2;
	node.key = key5;
	tree.insert(node);

	// check number of nodes in tree
	BOOST_CHECK_EQUAL( tree.size(), 5 );
	BOOST_CHECK_EQUAL( tree.height(), 2 );

	// check red-black properties
	check_red_black_props(tree);
}

BOOST_AUTO_TEST_CASE( insert_right_child_right_uncle_black_grandparent_not_root )
{
	// create red-black tree
	rb_tree tree;
	rb_node node;

	// insert data
	int key1 = 10;
	node.key = key1;
	tree.insert(node);

	int key2 = 8;
	node.key = key2;
	tree.insert(node);

	int key3 = 6;
	node.key = key3;
	tree.insert(node);

	int key4 = 2;
	node.key = key4;
	tree.insert(node);

	int key5 = 4;
	node.key = key5;
	tree.insert(node);

	// check number of nodes in tree
	BOOST_CHECK_EQUAL( tree.size(), 5 );
	BOOST_CHECK_EQUAL( tree.height(), 2 );

	// check red-black properties
	check_red_black_props(tree);
}

BOOST_AUTO_TEST_CASE( insert_right_child_left_uncle_black_grandparent_not_root )
{
	// create red-black tree
	rb_tree tree;
	rb_node node;

	// insert data
	int key1 = 2;
	node.key = key1;
	tree.insert(node);

	int key2 = 4;
	node.key = key2;
	tree.insert(node);

	int key3 = 6;
	node.key = key3;
	tree.insert(node);

	int key4 = 8;
	node.key = key4;
	tree.insert(node);

	int key5 = 10;
	node.key = key5;
	tree.insert(node);

	// check number of nodes in tree
	BOOST_CHECK_EQUAL( tree.size(), 5 );
	BOOST_CHECK_EQUAL( tree.height(), 2 );

	// check red-black properties
	check_red_black_props(tree);
}

BOOST_AUTO_TEST_CASE( insert_left_child_left_uncle_black_grandparent_not_root )
{
	// create red-black tree
	rb_tree tree;
	rb_node node;

	// insert data
	int key1 = 2;
	node.key = key1;
	tree.insert(node);

	int key2 = 4;
	node.key = key2;
	tree.insert(node);

	int key3 = 6;
	node.key = key3;
	tree.insert(node);

	int key4 = 10;
	node.key = key4;
	tree.insert(node);

	int key5 = 8;
	node.key = key5;
	tree.insert(node);

	// check number of nodes in tree
	BOOST_CHECK_EQUAL( tree.size(), 5 );
	BOOST_CHECK_EQUAL( tree.height(), 2 );

	// check red-black properties
	check_red_black_props(tree);
}


BOOST_AUTO_TEST_CASE( insert_many_nodes_positive_keys )
{
	// create red-black tree
	rb_tree tree;
	rb_node node;

	// insert keys in this order: 100, 1, 99, 2, 98, 3, ...
	int range = 100;
	for (int i = 0; i < range/2; ++i) {

		node.key = range - i;
		tree.insert(node);
		node.key = 1 + i;
		tree.insert(node);
	}

	// check number of nodes in tree
	BOOST_CHECK_EQUAL( tree.size(), 100 );

	// check red-black properties
	check_red_black_props(tree);
}


BOOST_AUTO_TEST_CASE( insert_many_nodes_negative_keys )
{
	// create red-black tree
	rb_tree tree;
	rb_node node;

	// insert keys in this order: -100, -1, -99, -2, -98, -3, ...
	int range = -100;
	for (int i = 0; i > range/2; --i) {

		node.key = range - i;
		tree.insert(node);
		node.key = - 1 + i;
		tree.insert(node);
	}

	// check number of nodes in tree
	BOOST_CHECK_EQUAL( tree.size(), 100 );

	// check red-black properties
	check_red_black_props(tree);
}


BOOST_AUTO_TEST_CASE( insert_many_nodes_mix_keys )
{
	// create red-black tree
	rb_tree tree;
	rb_node node;

	// insert keys in this order: -100, -1, -99, -2, -98, -3, ...
	int range = 100;
	for (int i = 0; i < range/2; ++i) {

		node.key = range/2 - i;
		tree.insert(node);
		node.key = 1 - range/2 + i;
		tree.insert(node);
	}

	// check number of nodes in tree
	BOOST_CHECK_EQUAL( tree.size(), 100 );

	// check red-black properties
	check_red_black_props(tree);
}


BOOST_AUTO_TEST_CASE( fetch_existing_node )
{
	// create red-black tree
	rb_tree tree;
	rb_node node;

	// insert keys in this order
	int range = 10;
	for (int i = 0; i < range; ++i) {

		node.key = i;
		tree.insert(node);
	}

	// fetch node
	int key = 5;
	const std::shared_ptr<const rb_node> result = tree.fetch(key);

	BOOST_REQUIRE(result != tree.get_nil());
	BOOST_CHECK_EQUAL(result->key, key);
}


BOOST_AUTO_TEST_CASE( fetch_nonexisting_node )
{
	// create red-black tree
	rb_tree tree;
	rb_node node;

	// insert keys in this order
	int range = 10;
	for (int i = 0; i < range; ++i) {

		node.key = i;
		tree.insert(node);
	}

	// fetch node
	int key = 11;
	const std::shared_ptr<const rb_node> result = tree.fetch(key);

	BOOST_CHECK_EQUAL(result, tree.get_nil());
}


/**
 * Checks red-black properties of a red-black tree.
 *
 * @param red-black tree to check
 */
static void check_red_black_props(const rb_tree& tree) {

	// red-black property: the root is black
	BOOST_CHECK( tree.size() == 0 || tree.get_root()->color == black );

	// red-black property: every leaf node is black
	BOOST_CHECK_EQUAL( tree.get_nil()->color, black );

	// red-black property: red nodes must have black children
	BOOST_CHECK( have_red_nodes_black_children(tree) );

	// red-black property: for each node, all simple paths to leaf nodes contain the same number of black nodes
	BOOST_CHECK( have_simple_paths_same_number_of_black_nodes(tree) );

	// consequence of red-black properties:
	// A red-black tree with n internal nodes has height at most 2*log2(n + 1)
	// cf. Cormen et al.: Introduction to Algorithms
	BOOST_CHECK( !(tree.height() > 2*std::log2(tree.size() + 1)) );

}

/**
 * Checks that red nodes have exclusively black children.
 *
 * @param
 * @return true if all red nodes have exclusively black children, false otherwise
 */
static bool check_red_nodes(const std::shared_ptr<rb_node> node, const std::shared_ptr<rb_node> nil) {

	if (node == nil) {

		return true;
	}
	else {

		// red-black property:
		// red nodes must have black children
		if (node->color == red) {

			if (node->left->color != black
				||
				node->right->color != black) {

				return false;
			}
		}

		return check_red_nodes(node->left, nil) && check_red_nodes(node->right, nil);
	}
}

/**
 * Checks that all red nodes in the red-black tree have exclusively black children.
 *
 * @param red-black tree
 * @return true if all red nodes have exclusively black children, false otherwise
 */
static bool have_red_nodes_black_children(const rb_tree& tree) {

	return check_red_nodes(tree.get_root(), tree.get_nil());
}

static bool check_paths(const std::shared_ptr<rb_node> node, int& cnt, const std::shared_ptr<rb_node> nil) {

	if (node == nil) {

		cnt = 0;
		return true;
	}
	else {

		int lcnt = 0;
		int rcnt = 0;
		bool lchk = check_paths(node->left, lcnt, nil);
		bool rchk = check_paths(node->right, rcnt, nil);

		// no problems in left and right subtrees
		if (!lchk || !rchk) {
			return false;
		}

		// red-black property:
		// all simple paths from a node to descendant leaves
		// contain the same number of black nodes
		if (lcnt != rcnt) {
			return false;
		}

		// increase count of black nodes
		cnt = lcnt;
		if (node->color == black) {
			++cnt;
		}

		return true;
	}
}

static bool have_simple_paths_same_number_of_black_nodes(const rb_tree& tree) {

	int n = 0;
	return check_paths(tree.get_root(), n, tree.get_nil());
}

// EOF

