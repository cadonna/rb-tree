#include <iostream>
#include <string>
#include <boost/test/unit_test.hpp>
#include "../src/rb_tree_tmpl.hpp"
#include "rb_tree_tmpl_test.hpp"

BOOST_AUTO_TEST_CASE( node_constructor_int )
{
	rb_node<int> node;
	BOOST_CHECK_EQUAL(node.key, 0);
	BOOST_CHECK_EQUAL(node.color, black);
	BOOST_CHECK(node.parent == nullptr);
	BOOST_CHECK(node.left == nullptr);
	BOOST_CHECK(node.right == nullptr);
}

BOOST_AUTO_TEST_CASE( node_constructor_string )
{
	rb_node<std::string> node;
	BOOST_CHECK_EQUAL(node.key, "");
	BOOST_CHECK_EQUAL(node.color, black);
	BOOST_CHECK(node.parent == nullptr);
	BOOST_CHECK(node.left == nullptr);
	BOOST_CHECK(node.right == nullptr);
}


BOOST_AUTO_TEST_CASE( node_equality_op_int )
{
	rb_node<int> node1;
	rb_node<int> node2;
	BOOST_CHECK(node1 == node2);
	BOOST_CHECK(node1 == node1);

	rb_node<int> node3;
	node3.key = 1;
	BOOST_CHECK(!(node3 == node1));

	rb_node<int> node4;
	node4.color = red;
	BOOST_CHECK(!(node4 == node1));

	std::shared_ptr<rb_node<int> > left = std::make_shared<rb_node<int> >();
	rb_node<int> node5;
	node5.left = left;
	BOOST_CHECK(!(node5 == node1));

	std::shared_ptr<rb_node<int> > right = std::make_shared<rb_node<int> >();
	rb_node<int> node6;
	node6.right = right;
	BOOST_CHECK(!(node6 == node1));
}

BOOST_AUTO_TEST_CASE( node_equality_op_string )
{
	rb_node<std::string> node1;
	rb_node<std::string> node2;
	BOOST_CHECK(node1 == node2);
	BOOST_CHECK(node1 == node1);

	rb_node<std::string> node3;
	node3.key = 1;
	BOOST_CHECK(!(node3 == node1));

	rb_node<std::string> node4;
	node4.color = red;
	BOOST_CHECK(!(node4 == node1));

	std::shared_ptr<rb_node<std::string> > left = std::make_shared<rb_node<std::string> >();
	rb_node<std::string> node5;
	node5.left = left;
	BOOST_CHECK(!(node5 == node1));

	std::shared_ptr<rb_node<std::string> > right = std::make_shared<rb_node<std::string> >();
	rb_node<std::string> node6;
	node6.right = right;
	BOOST_CHECK(!(node6 == node1));
}

BOOST_AUTO_TEST_CASE( tree_constructors_int )
{
	rb_tree<int> tree;
	BOOST_CHECK_EQUAL(tree.size(), 0);
	BOOST_CHECK_EQUAL(tree.height(), 0);
	BOOST_CHECK_EQUAL(tree.black_height(tree.get_root()), 0);
	BOOST_CHECK(tree.get_root() == tree.get_nil());

	// check red-black properties
	check_red_black_props(tree);
}

BOOST_AUTO_TEST_CASE( tree_constructors_string )
{
	rb_tree<std::string> tree;
	BOOST_CHECK_EQUAL(tree.size(), 0);
	BOOST_CHECK_EQUAL(tree.height(), 0);
	BOOST_CHECK_EQUAL(tree.black_height(tree.get_root()), 0);
	BOOST_CHECK(tree.get_root() == tree.get_nil());

	// check red-black properties
	check_red_black_props(tree);
}

BOOST_AUTO_TEST_CASE( insert_single_node_int)
{
	// create red-black tree
	rb_tree<int> tree;
	rb_node<int> node;

	// insert data
	int key1 = 2;
	node.key = key1;
	tree.insert(node);

	// check number of nodes in tree
	BOOST_CHECK_EQUAL( tree.size(), 1 );

	// check root
	BOOST_CHECK_EQUAL( tree.get_root()->key, key1 );
	BOOST_CHECK_EQUAL( tree.get_root()->parent, tree.get_nil() );
	BOOST_CHECK_EQUAL( tree.get_root()->left, tree.get_nil() );
	BOOST_CHECK_EQUAL( tree.get_root()->right, tree.get_nil() );

	// check red-black properties
	check_red_black_props(tree);
}

BOOST_AUTO_TEST_CASE( insert_single_node_string)
{
	// create red-black tree
	rb_tree<std::string> tree;
	rb_node<std::string> node;

	// insert data
	std::string key1 = "abc";
	node.key = key1;
	tree.insert(node);

	// check number of nodes in tree
	BOOST_CHECK_EQUAL( tree.size(), 1 );

	// check root
	BOOST_CHECK_EQUAL( tree.get_root()->key, key1 );
	BOOST_CHECK_EQUAL( tree.get_root()->parent, tree.get_nil() );
	BOOST_CHECK_EQUAL( tree.get_root()->left, tree.get_nil() );
	BOOST_CHECK_EQUAL( tree.get_root()->right, tree.get_nil() );

	// check red-black properties
	check_red_black_props(tree);
}

BOOST_AUTO_TEST_CASE( insert_check_return_value_int)
{
	// create red-black tree
	rb_tree<int> tree;
	rb_node<int> node;

	// insert data
	int key1 = 2;
	node.key = key1;
	bool ok1 = tree.insert(node);
	bool ok2 = tree.insert(node);

	// check
	BOOST_CHECK( ok1 );
	BOOST_CHECK( !ok2 );
}

BOOST_AUTO_TEST_CASE( insert_check_return_value_string)
{
	// create red-black tree
	rb_tree<std::string> tree;
	rb_node<std::string> node;

	// insert data
	std::string key1 = "aaa";
	node.key = key1;
	bool ok1 = tree.insert(node);
	bool ok2 = tree.insert(node);

	// check
	BOOST_CHECK( ok1 );
	BOOST_CHECK( !ok2 );
}

BOOST_AUTO_TEST_CASE( insert_left_child_right_uncle_red_int )
{
	// create red-black tree
	rb_tree<int> tree;
	rb_node<int> node;

	// insert data
	int key1 = 5;
	node.key = key1;
	tree.insert(node);

	int key2 = 4;
	node.key = key2;
	tree.insert(node);

	int key3 = 6;
	node.key = key3;
	tree.insert(node);

	int key4 = 3;
	node.key = key4;
	tree.insert(node);

	// check number of nodes in tree
	BOOST_CHECK_EQUAL( tree.size(), 4 );
	BOOST_CHECK_EQUAL( tree.height(), 2 );

	// check red-black properties
	check_red_black_props(tree);
}

BOOST_AUTO_TEST_CASE( insert_left_child_right_uncle_red_string )
{
	// create red-black tree
	rb_tree<std::string> tree;
	rb_node<std::string> node;

	// insert data
	std::string key1 = "eee";
	node.key = key1;
	tree.insert(node);

	std::string key2 = "ddd";
	node.key = key2;
	tree.insert(node);

	std::string key3 = "fff";
	node.key = key3;
	tree.insert(node);

	std::string key4 = "ccc";
	node.key = key4;
	tree.insert(node);

	// check number of nodes in tree
	BOOST_CHECK_EQUAL( tree.size(), 4 );
	BOOST_CHECK_EQUAL( tree.height(), 2 );

	// check red-black properties
	check_red_black_props(tree);
}

BOOST_AUTO_TEST_CASE( insert_right_child_right_uncle_red_int )
{
	// create red-black tree
	rb_tree<int> tree;
	rb_node<int> node;

	// insert data
	int key1 = 5;
	node.key = key1;
	tree.insert(node);

	int key2 = 3;
	node.key = key2;
	tree.insert(node);

	int key3 = 6;
	node.key = key3;
	tree.insert(node);

	int key4 = 4;
	node.key = key4;
	tree.insert(node);

	// check number of nodes in tree
	BOOST_CHECK_EQUAL( tree.size(), 4 );
	BOOST_CHECK_EQUAL( tree.height(), 2 );

	// check red-black properties
	check_red_black_props(tree);
}

BOOST_AUTO_TEST_CASE( insert_right_child_right_uncle_red_string )
{
	// create red-black tree
	rb_tree<std::string> tree;
	rb_node<std::string> node;

	// insert data
	std::string key1 = "eee";
	node.key = key1;
	tree.insert(node);

	std::string key2 = "ccc";
	node.key = key2;
	tree.insert(node);

	std::string key3 = "fff";
	node.key = key3;
	tree.insert(node);

	std::string key4 = "ddd";
	node.key = key4;
	tree.insert(node);

	// check number of nodes in tree
	BOOST_CHECK_EQUAL( tree.size(), 4 );
	BOOST_CHECK_EQUAL( tree.height(), 2 );

	// check red-black properties
	check_red_black_props(tree);
}

BOOST_AUTO_TEST_CASE( insert_right_child_left_uncle_red_int )
{
	// create red-black tree
	rb_tree<int> tree;
	rb_node<int> node;

	// insert data
	int key1 = 5;
	node.key = key1;
	tree.insert(node);

	int key2 = 4;
	node.key = key2;
	tree.insert(node);

	int key3 = 6;
	node.key = key3;
	tree.insert(node);

	int key4 = 7;
	node.key = key4;
	tree.insert(node);

	// check number of nodes in tree
	BOOST_CHECK_EQUAL( tree.size(), 4 );
	BOOST_CHECK_EQUAL( tree.height(), 2 );

	// check red-black properties
	check_red_black_props(tree);
}

BOOST_AUTO_TEST_CASE( insert_right_child_left_uncle_red_string )
{
	// create red-black tree
	rb_tree<std::string> tree;
	rb_node<std::string> node;

	// insert data
	std::string key1 = "eee";
	node.key = key1;
	tree.insert(node);

	std::string key2 = "ddd";
	node.key = key2;
	tree.insert(node);

	std::string key3 = "fff";
	node.key = key3;
	tree.insert(node);

	std::string key4 = "ggg";
	node.key = key4;
	tree.insert(node);

	// check number of nodes in tree
	BOOST_CHECK_EQUAL( tree.size(), 4 );
	BOOST_CHECK_EQUAL( tree.height(), 2 );

	// check red-black properties
	check_red_black_props(tree);
}

BOOST_AUTO_TEST_CASE( insert_left_child_left_uncle_red_int )
{
	// create red-black tree
	rb_tree<int> tree;
	rb_node<int> node;

	// insert data
	int key1 = 5;
	node.key = key1;
	tree.insert(node);

	int key2 = 4;
	node.key = key2;
	tree.insert(node);

	int key3 = 7;
	node.key = key3;
	tree.insert(node);

	int key4 = 6;
	node.key = key4;
	tree.insert(node);

	// check number of nodes in tree
	BOOST_CHECK_EQUAL( tree.size(), 4 );
	BOOST_CHECK_EQUAL( tree.height(), 2 );

	// check red-black properties
	check_red_black_props(tree);
}

BOOST_AUTO_TEST_CASE( insert_left_child_left_uncle_red_string )
{
	// create red-black tree
	rb_tree<std::string> tree;
	rb_node<std::string> node;

	// insert data
	std::string key1 = "eee";
	node.key = key1;
	tree.insert(node);

	std::string key2 = "ddd";
	node.key = key2;
	tree.insert(node);

	std::string key3 = "ggg";
	node.key = key3;
	tree.insert(node);

	std::string key4 = "fff";
	node.key = key4;
	tree.insert(node);

	// check number of nodes in tree
	BOOST_CHECK_EQUAL( tree.size(), 4 );
	BOOST_CHECK_EQUAL( tree.height(), 2 );

	// check red-black properties
	check_red_black_props(tree);
}

BOOST_AUTO_TEST_CASE( insert_left_child_left_uncle_black_grandparent_root_int )
{
	// create red-black tree
	rb_tree<int> tree;
	rb_node<int> node;

	// insert data
	int key1 = 5;
	node.key = key1;
	tree.insert(node);

	int key2 = 7;
	node.key = key2;
	tree.insert(node);

	int key3 = 6;
	node.key = key3;
	tree.insert(node);

	// check number of nodes in tree
	BOOST_CHECK_EQUAL( tree.size(), 3 );
	BOOST_CHECK_EQUAL( tree.height(), 1 );

	// check red-black properties
	check_red_black_props(tree);
}

BOOST_AUTO_TEST_CASE( insert_left_child_left_uncle_black_grandparent_root_string )
{
	// create red-black tree
	rb_tree<std::string> tree;
	rb_node<std::string> node;

	// insert data
	std::string key1 = "eee";
	node.key = key1;
	tree.insert(node);

	std::string key2 = "ggg";
	node.key = key2;
	tree.insert(node);

	std::string key3 = "fff";
	node.key = key3;
	tree.insert(node);

	// check number of nodes in tree
	BOOST_CHECK_EQUAL( tree.size(), 3 );
	BOOST_CHECK_EQUAL( tree.height(), 1 );

	// check red-black properties
	check_red_black_props(tree);
}

BOOST_AUTO_TEST_CASE( insert_right_child_left_uncle_black_grandparent_root_int )
{
	// create red-black tree
	rb_tree<int> tree;
	rb_node<int> node;

	// insert data
	int key1 = 5;
	node.key = key1;
	tree.insert(node);

	int key2 = 6;
	node.key = key2;
	tree.insert(node);

	int key3 = 7;
	node.key = key3;
	tree.insert(node);

	// check number of nodes in tree
	BOOST_CHECK_EQUAL( tree.size(), 3 );
	BOOST_CHECK_EQUAL( tree.height(), 1 );

	// check red-black properties
	check_red_black_props(tree);
}

BOOST_AUTO_TEST_CASE( insert_right_child_left_uncle_black_grandparent_root_string )
{
	// create red-black tree
	rb_tree<std::string> tree;
	rb_node<std::string> node;

	// insert data
	std::string key1 = "eee";
	node.key = key1;
	tree.insert(node);

	std::string key2 = "fff";
	node.key = key2;
	tree.insert(node);

	std::string key3 = "ggg";
	node.key = key3;
	tree.insert(node);

	// check number of nodes in tree
	BOOST_CHECK_EQUAL( tree.size(), 3 );
	BOOST_CHECK_EQUAL( tree.height(), 1 );

	// check red-black properties
	check_red_black_props(tree);
}

BOOST_AUTO_TEST_CASE( insert_left_child_right_uncle_black_grandparent_root_int )
{
	// create red-black tree
	rb_tree<int> tree;
	rb_node<int> node;

	// insert data
	int key1 = 5;
	node.key = key1;
	tree.insert(node);

	int key2 = 4;
	node.key = key2;
	tree.insert(node);

	int key3 = 3;
	node.key = key3;
	tree.insert(node);

	// check number of nodes in tree
	BOOST_CHECK_EQUAL( tree.size(), 3 );
	BOOST_CHECK_EQUAL( tree.height(), 1 );

	// check red-black properties
	check_red_black_props(tree);
}

BOOST_AUTO_TEST_CASE( insert_left_child_right_uncle_black_grandparent_root_string )
{
	// create red-black tree
	rb_tree<std::string> tree;
	rb_node<std::string> node;

	// insert data
	std::string key1 = "eee";
	node.key = key1;
	tree.insert(node);

	std::string key2 = "ddd";
	node.key = key2;
	tree.insert(node);

	std::string key3 = "ccc";
	node.key = key3;
	tree.insert(node);

	// check number of nodes in tree
	BOOST_CHECK_EQUAL( tree.size(), 3 );
	BOOST_CHECK_EQUAL( tree.height(), 1 );

	// check red-black properties
	check_red_black_props(tree);
}

BOOST_AUTO_TEST_CASE( insert_right_child_right_uncle_black_grandparent_root_int )
{
	// create red-black tree
	rb_tree<int> tree;
	rb_node<int> node;

	// insert data
	int key1 = 5;
	node.key = key1;
	tree.insert(node);

	int key2 = 3;
	node.key = key2;
	tree.insert(node);

	int key3 = 4;
	node.key = key3;
	tree.insert(node);

	// check number of nodes in tree
	BOOST_CHECK_EQUAL( tree.size(), 3 );
	BOOST_CHECK_EQUAL( tree.height(), 1 );

	// check red-black properties
	check_red_black_props(tree);
}

BOOST_AUTO_TEST_CASE( insert_right_child_right_uncle_black_grandparent_root_string )
{
	// create red-black tree
	rb_tree<std::string> tree;
	rb_node<std::string> node;

	// insert data
	std::string key1 = "eee";
	node.key = key1;
	tree.insert(node);

	std::string key2 = "ccc";
	node.key = key2;
	tree.insert(node);

	std::string key3 = "ddd";
	node.key = key3;
	tree.insert(node);

	// check number of nodes in tree
	BOOST_CHECK_EQUAL( tree.size(), 3 );
	BOOST_CHECK_EQUAL( tree.height(), 1 );

	// check red-black properties
	check_red_black_props(tree);
}

BOOST_AUTO_TEST_CASE( insert_left_child_right_uncle_black_grandparent_not_root_int )
{
	// create red-black tree
	rb_tree<int> tree;
	rb_node<int> node;

	// insert data
	int key1 = 10;
	node.key = key1;
	tree.insert(node);

	int key2 = 8;
	node.key = key2;
	tree.insert(node);

	int key3 = 6;
	node.key = key3;
	tree.insert(node);

	int key4 = 4;
	node.key = key4;
	tree.insert(node);

	int key5 = 2;
	node.key = key5;
	tree.insert(node);

	// check number of nodes in tree
	BOOST_CHECK_EQUAL( tree.size(), 5 );
	BOOST_CHECK_EQUAL( tree.height(), 2 );

	// check red-black properties
	check_red_black_props(tree);
}

BOOST_AUTO_TEST_CASE( insert_left_child_right_uncle_black_grandparent_not_root_string )
{
	// create red-black tree
	rb_tree<std::string> tree;
	rb_node<std::string> node;

	// insert data
	std::string key1 = "jjj";
	node.key = key1;
	tree.insert(node);

	std::string key2 = "hhh";
	node.key = key2;
	tree.insert(node);

	std::string key3 = "fff";
	node.key = key3;
	tree.insert(node);

	std::string key4 = "ddd";
	node.key = key4;
	tree.insert(node);

	std::string key5 = "bbb";
	node.key = key5;
	tree.insert(node);

	// check number of nodes in tree
	BOOST_CHECK_EQUAL( tree.size(), 5 );
	BOOST_CHECK_EQUAL( tree.height(), 2 );

	// check red-black properties
	check_red_black_props(tree);
}

BOOST_AUTO_TEST_CASE( insert_right_child_right_uncle_black_grandparent_not_root_int )
{
	// create red-black tree
	rb_tree<int> tree;
	rb_node<int> node;

	// insert data
	int key1 = 10;
	node.key = key1;
	tree.insert(node);

	int key2 = 8;
	node.key = key2;
	tree.insert(node);

	int key3 = 6;
	node.key = key3;
	tree.insert(node);

	int key4 = 2;
	node.key = key4;
	tree.insert(node);

	int key5 = 4;
	node.key = key5;
	tree.insert(node);

	// check number of nodes in tree
	BOOST_CHECK_EQUAL( tree.size(), 5 );
	BOOST_CHECK_EQUAL( tree.height(), 2 );

	// check red-black properties
	check_red_black_props(tree);
}

BOOST_AUTO_TEST_CASE( insert_right_child_right_uncle_black_grandparent_not_root_string )
{
	// create red-black tree
	rb_tree<std::string> tree;
	rb_node<std::string> node;

	// insert data
	std::string key1 = "jjj";
	node.key = key1;
	tree.insert(node);

	std::string key2 = "hhh";
	node.key = key2;
	tree.insert(node);

	std::string key3 = "fff";
	node.key = key3;
	tree.insert(node);

	std::string key4 = "bbb";
	node.key = key4;
	tree.insert(node);

	std::string key5 = "ddd";
	node.key = key5;
	tree.insert(node);

	// check number of nodes in tree
	BOOST_CHECK_EQUAL( tree.size(), 5 );
	BOOST_CHECK_EQUAL( tree.height(), 2 );

	// check red-black properties
	check_red_black_props(tree);
}

BOOST_AUTO_TEST_CASE( insert_right_child_left_uncle_black_grandparent_not_root_int )
{
	// create red-black tree
	rb_tree<int> tree;
	rb_node<int> node;

	// insert data
	int key1 = 2;
	node.key = key1;
	tree.insert(node);

	int key2 = 4;
	node.key = key2;
	tree.insert(node);

	int key3 = 6;
	node.key = key3;
	tree.insert(node);

	int key4 = 8;
	node.key = key4;
	tree.insert(node);

	int key5 = 10;
	node.key = key5;
	tree.insert(node);

	// check number of nodes in tree
	BOOST_CHECK_EQUAL( tree.size(), 5 );
	BOOST_CHECK_EQUAL( tree.height(), 2 );

	// check red-black properties
	check_red_black_props(tree);
}

BOOST_AUTO_TEST_CASE( insert_right_child_left_uncle_black_grandparent_not_root_string )
{
	// create red-black tree
	rb_tree<std::string> tree;
	rb_node<std::string> node;

	// insert data
	std::string key1 = "bbb";
	node.key = key1;
	tree.insert(node);

	std::string key2 = "ddd";
	node.key = key2;
	tree.insert(node);

	std::string key3 = "fff";
	node.key = key3;
	tree.insert(node);

	std::string key4 = "hhh";
	node.key = key4;
	tree.insert(node);

	std::string key5 = "jjj";
	node.key = key5;
	tree.insert(node);

	// check number of nodes in tree
	BOOST_CHECK_EQUAL( tree.size(), 5 );
	BOOST_CHECK_EQUAL( tree.height(), 2 );

	// check red-black properties
	check_red_black_props(tree);
}

BOOST_AUTO_TEST_CASE( insert_left_child_left_uncle_black_grandparent_not_root_int )
{
	// create red-black tree
	rb_tree<int> tree;
	rb_node<int> node;

	// insert data
	int key1 = 2;
	node.key = key1;
	tree.insert(node);

	int key2 = 4;
	node.key = key2;
	tree.insert(node);

	int key3 = 6;
	node.key = key3;
	tree.insert(node);

	int key4 = 10;
	node.key = key4;
	tree.insert(node);

	int key5 = 8;
	node.key = key5;
	tree.insert(node);

	// check number of nodes in tree
	BOOST_CHECK_EQUAL( tree.size(), 5 );
	BOOST_CHECK_EQUAL( tree.height(), 2 );

	// check red-black properties
	check_red_black_props(tree);
}


BOOST_AUTO_TEST_CASE( insert_left_child_left_uncle_black_grandparent_not_root_string )
{
	// create red-black tree
	rb_tree<std::string> tree;
	rb_node<std::string> node;

	// insert data
	std::string key1 = "bbb";
	node.key = key1;
	tree.insert(node);

	std::string key2 = "ddd";
	node.key = key2;
	tree.insert(node);

	std::string key3 = "fff";
	node.key = key3;
	tree.insert(node);

	std::string key4 = "jjj";
	node.key = key4;
	tree.insert(node);

	std::string key5 = "hhh";
	node.key = key5;
	tree.insert(node);

	// check number of nodes in tree
	BOOST_CHECK_EQUAL( tree.size(), 5 );
	BOOST_CHECK_EQUAL( tree.height(), 2 );

	// check red-black properties
	check_red_black_props(tree);
}


BOOST_AUTO_TEST_CASE( insert_many_nodes_positive_int_keys )
{
	// create red-black tree
	rb_tree<int> tree;
	rb_node<int> node;

	// insert keys in this order: 100, 1, 99, 2, 98, 3, ...
	int range = 100;
	for (int i = 0; i < range/2; ++i) {

		node.key = range - i;
		tree.insert(node);
		node.key = 1 + i;
		tree.insert(node);
	}

	// check number of nodes in tree
	BOOST_CHECK_EQUAL( tree.size(), 100 );

	// check red-black properties
	check_red_black_props(tree);
}


BOOST_AUTO_TEST_CASE( insert_many_nodes_negative_int_keys )
{
	// create red-black tree
	rb_tree<int> tree;
	rb_node<int> node;

	// insert keys in this order: -100, -1, -99, -2, -98, -3, ...
	int range = -100;
	for (int i = 0; i > range/2; --i) {

		node.key = range - i;
		tree.insert(node);
		node.key = - 1 + i;
		tree.insert(node);
	}

	// check number of nodes in tree
	BOOST_CHECK_EQUAL( tree.size(), 100 );

	// check red-black properties
	check_red_black_props(tree);
}


BOOST_AUTO_TEST_CASE( insert_many_nodes_mix_int_keys )
{
	// create red-black tree
	rb_tree<int> tree;
	rb_node<int> node;

	// insert keys in this order: -100, -1, -99, -2, -98, -3, ...
	int range = 100;
	for (int i = 0; i < range/2; ++i) {

		node.key = range/2 - i;
		tree.insert(node);
		node.key = 1 - range/2 + i;
		tree.insert(node);
	}

	// check number of nodes in tree
	BOOST_CHECK_EQUAL( tree.size(), 100 );

	// check red-black properties
	check_red_black_props(tree);
}


BOOST_AUTO_TEST_CASE( insert_many_nodes_string_keys )
{
	// create red-black tree
	rb_tree<std::string> tree;
	rb_node<std::string> node;

	// insert keys in this order: -100, -1, -99, -2, -98, -3, ...
	int range = 100;
	for (int i = 0; i < range/2; ++i) {

		node.key = std::to_string(range/2 - i);
		tree.insert(node);
		node.key = std::to_string(1 - range/2 + i);
		tree.insert(node);
	}

	// check number of nodes in tree
	BOOST_CHECK_EQUAL( tree.size(), 100 );

	// check red-black properties
	check_red_black_props(tree);
}


BOOST_AUTO_TEST_CASE( fetch_existing_node_int )
{
	// create red-black tree
	rb_tree<int> tree;
	rb_node<int> node;

	// insert keys in this order
	int range = 10;
	for (int i = 0; i < range; ++i) {

		node.key = i;
		tree.insert(node);
	}

	// fetch node
	int key = 5;
	const std::shared_ptr<const rb_node<int> > result = tree.fetch(key);

	BOOST_REQUIRE(result != tree.get_nil());
	BOOST_CHECK_EQUAL(result->key, key);
}


BOOST_AUTO_TEST_CASE( fetch_existing_node_string )
{
	// create red-black tree
	rb_tree<std::string> tree;
	rb_node<std::string> node;

	// insert keys in this order
	int range = 10;
	for (int i = 0; i < range; ++i) {

		node.key = std::to_string(i);
		tree.insert(node);
	}

	// fetch node
	std::string key = "5";
	const std::shared_ptr<const rb_node<std::string> > result = tree.fetch(key);

	BOOST_REQUIRE(result != tree.get_nil());
	BOOST_CHECK_EQUAL(result->key, key);
}


BOOST_AUTO_TEST_CASE( fetch_nonexisting_node_int )
{
	// create red-black tree
	rb_tree<int> tree;
	rb_node<int> node;

	// insert keys in this order
	int range = 10;
	for (int i = 0; i < range; ++i) {

		node.key = i;
		tree.insert(node);
	}

	// fetch node
	int key = 11;
	const std::shared_ptr<const rb_node<int> > result = tree.fetch(key);

	BOOST_CHECK_EQUAL(result, tree.get_nil());
}

BOOST_AUTO_TEST_CASE( fetch_nonexisting_node_string )
{
	// create red-black tree
	rb_tree<std::string> tree;
	rb_node<std::string> node;

	// insert keys in this order
	int range = 10;
	for (int i = 0; i < range; ++i) {

		node.key = std::to_string(i);
		tree.insert(node);
	}

	// fetch node
	std::string key = "11";
	const std::shared_ptr<const rb_node<std::string> > result = tree.fetch(key);

	BOOST_CHECK_EQUAL(result, tree.get_nil());
}
